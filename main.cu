#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cufft.h>
#include <math.h>

#define FFTSAMPLE 1023 //complex sample size == (int)(FFTSAMPLE / 2) + 1
#define BATCH 1500

typedef int wav_type;

typedef struct {
  int       len;
  char  header[44];
  wav_type *data;
} wav;

typedef float real;
typedef cufftComplex complex;

wav get_wav(char *file) {
  FILE *fd;
  fd = fopen(file, "r");
  char header[44];
  int chunk_size;
  int i;
  int num;
  int counter = 0;
  wav_type *data;
  for(i = 0; i < 44; i ++) {
    header[i] = fgetc(fd);
  }

  memcpy(&chunk_size,
	 &header[4],
	 sizeof(chunk_size)); //extract length of wav  
  data = (wav_type*)malloc((chunk_size - 36)
			   * sizeof(wav_type));
  
  do {
    wav_type val;
    num = fread((void*)(&val), sizeof(wav_type), 1, fd);
    data[counter] = val;
    counter ++;
  } while (num > 0);
  wav wav_data;
  wav_data.len = counter;
  memcpy(wav_data.header, header, 44 * sizeof(char));
  wav_data.data = data;
  fclose(fd);
  return wav_data;
}

void write_wav(wav data, char *file) {
  FILE *fd;
  fd = fopen(file, "w");
  fwrite(data.header, sizeof(char), 44, fd);
  fwrite(data.data, sizeof(wav_type), data.len, fd);
  fclose(fd);
}

int fill(int* arr, int count, int val) {
  int i;
  for(i = 0; i < count; i ++)
    arr[i] = val;
  return 0;
}

complex *forward(real *data, int samples, int howmany) {
  cufftHandle f_plan;
  complex *gpu_fwd_ft;
  real *gpu_data;
  int len = samples * howmany;
  int rank = 1;
  int idist = (int)(len / howmany);
  int odist = (int)(idist / 2) + 1;
  int *arr_lens = (int*)malloc(howmany * sizeof(int));
  fill(arr_lens, howmany, idist);

  int data_len = sizeof(real) * howmany * idist;
  
  cudaMalloc((void**)&gpu_data, data_len);

  cudaMemcpy(gpu_data, data, data_len, cudaMemcpyHostToDevice);

  cudaMalloc((void**)&gpu_fwd_ft , sizeof(complex) 
	     * howmany 
	     * odist);

  cufftPlanMany(&f_plan, rank, arr_lens,
		NULL, 1, idist, //ignored
		NULL, 1, odist, //ignored
		CUFFT_R2C, howmany);

  cufftExecR2C(f_plan, gpu_data, gpu_fwd_ft);
  
  cufftDestroy(f_plan);
  cudaFree(gpu_data);

  return gpu_fwd_ft;
}

real *backward(complex *data, int samples, int howmany) {
  cufftHandle b_plan;
  real *gpu_bwd_ft;
  int len = samples * howmany;
  int rank = 1;
  int *arr_lens = (int*)malloc(howmany * sizeof(int));
  int odist = (int)(len / howmany);
  int idist = (int)(odist / 2) + 1;
  fill(arr_lens, howmany, odist);
  int result_len = sizeof(real) * howmany * odist;

  real *result = (real*)malloc(result_len);

  cudaMalloc((void**)&gpu_bwd_ft, result_len);

  cufftPlanMany(&b_plan, rank, arr_lens,
		NULL, 1, idist,
		NULL, 1, odist,
		CUFFT_C2R, howmany);

  cufftExecC2R(b_plan, data, gpu_bwd_ft);
  
  cudaMemcpy(result, gpu_bwd_ft, result_len, cudaMemcpyDeviceToHost);
  cufftDestroy(b_plan);

  cudaFree(gpu_bwd_ft);
  cudaFree(data);

  return result;
}

__global__ void gauss_filter(complex *data) {
  int i = blockIdx.x;
  int j = threadIdx.x;

  int smpl_size = blockDim.x;
  //some magic constants
  float scale = 0.1;
  float a = 10.0;
  float b = 0.2 * scale;
  float c = 30.0;
 
  
  float x = (float) j / smpl_size * scale;// j --> [0..scale]
  float mul;
  mul = a * powf(2.71828, powf(x - b,2) / c);
  
  data[i * smpl_size + j].x *= mul;
  data[i * smpl_size + j].y *= mul;
  //  data[i * smpl_size + j][1] * mul;
  

}
//gaussian filter
int filter(complex *spectr, int samples, 
		int howmany) {

  printf("howmany: %d sample_size %d \n",howmany, samples); 
  gauss_filter<<<1024 * BATCH, 512>>>(spectr);
  
  return 0;
}

int main(int argc, char *argv[]) {
  char out_name[512];
  char* filename;
  if(argc != 2) {
    printf("wrong arguments count\n");
    return 1;
  }
  else 
    filename = argv[1];

  snprintf(out_name, sizeof(out_name), "%s%s", "gpu_", filename);
  
  wav wave = get_wav(filename);

  real *data = (real*)malloc(wave.len * sizeof(real));
  int samples = FFTSAMPLE;
  int howmany = (int)(wave.len / samples);
  int i;
  
  for(i = 0; i < samples * howmany; i ++) 
    data[i] = (real)wave.data[i] / samples;
 

  complex *f_ft = forward(data, samples, howmany);
  gauss_filter<<<howmany, (int)(samples / 2) + 1 >>>(f_ft);
  real    *b_ft = backward(f_ft, samples, howmany);

  for(i = 0; i < samples * howmany; i ++) {
    wave.data[i] = (wav_type)(b_ft[i]);
  }
  write_wav(wave, out_name);
  
  return 0;
}
