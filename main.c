#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fftw3.h>
#include <math.h>

#define FFTSAMPLE 1023

typedef int wav_type;

typedef struct {
  int       len;
  char  header[44];
  wav_type *data;
} wav;

typedef float real;
typedef fftwf_complex complex;

wav get_wav(char *file) {
  FILE *fd;
  fd = fopen(file, "r");
  char header[44];
  int chunk_size;
  int i;
  int num;
  int counter = 0;
  wav_type *data;
  for(i = 0; i < 44; i ++) {
    header[i] = fgetc(fd);
  }

  memcpy(&chunk_size,
	 &header[4],
	 sizeof(chunk_size)); //extract length of wav  
  data = (wav_type*)malloc((chunk_size - 36)
			   * sizeof(wav_type));
  
  do {
    wav_type val;
    num = fread((void*)(&val), sizeof(wav_type), 1, fd);
    data[counter] = val;
    counter ++;
  } while (num > 0);
  wav wav_data;
  wav_data.len = counter;
  memcpy(wav_data.header, header, 44 * sizeof(char));
  wav_data.data = data;
  fclose(fd);
  return wav_data;
}

void write_wav(wav data, char *file) {
  FILE *fd;
  fd = fopen(file, "w");
  fwrite(data.header, sizeof(char), 44, fd);
  fwrite(data.data, sizeof(wav_type), data.len, fd);
  fclose(fd);
}

int fill(int* arr, int count, int val) {
  int i;
  for(i = 0; i < count; i ++)
    arr[i] = val;
  return 0;
}

complex *forward(real *data, int samples, int howmany) {
  fftwf_plan f_plan;
  int len = samples * howmany;
  int *arr_lens = (int*)malloc(howmany * sizeof(int));

  int rank = 1;
  int idist = (int)(len / howmany);
  fill(arr_lens, howmany, idist);
  int odist = (int)(idist / 2) + 1;
  complex *f_ft = (complex*)malloc(odist 
   				   * howmany 
   				   * sizeof(complex));

  f_plan = fftwf_plan_many_dft_r2c(1, 
				   arr_lens,
				   howmany,
				   data, NULL, 1, idist,
				   f_ft, NULL, 1, odist,
				   FFTW_ESTIMATE);
				   
  fftwf_execute(f_plan);
  free(arr_lens);
  return f_ft;
}

real *backward(complex *data, int samples, int howmany) {
  fftwf_plan b_plan;
  int len = samples * howmany;
  int *arr_lens = (int*)malloc(howmany * sizeof(int));
  int rank = 1;
  int odist = (int)(len / howmany);
  int idist = (int)(odist / 2) + 1;
  fill(arr_lens, howmany, odist);
  real *b_ft = (real*)malloc(odist 
			     * howmany 
			     * sizeof(real));

  b_plan = fftwf_plan_many_dft_c2r(1, 
				   arr_lens,
				   howmany,
				   data, NULL, 1, idist,
				   b_ft, NULL, 1, odist,
				   FFTW_ESTIMATE);
				   
  fftwf_execute(b_plan);
  free(arr_lens);
  return b_ft;
}

//gaussian filter
int filter(complex *spectr, int samples, 
		int howmany) {
  int i,j;
  float scale = 0.1;
  float a = 10.0;
  float b = 0.2 * scale;
  float c = 30.0;

  for(i = 0; i < howmany; i++) {
    for(j = 0; j < samples; j ++) {
      float x = (float) j / samples * scale;//[0..scale]
      float mul =1.0 / (1 + x * x);
      mul = a * pow(2.71828, pow(x - b, 2) / c);
      spectr[i * samples + j][0] = 
	spectr[i * samples + j][0] * mul;
      spectr[i * samples + j][1] = 
	spectr[i * samples + j][1] * mul;
    }
  }

  return 0;
}

int main(int argc, char *argv[]) {
  char* filename;
  char out_name[512];
  if(argc != 2) {
    printf("wrong arguments count\n");
    return 1;
  }
  else
    filename = argv[1];

  snprintf(out_name, sizeof(out_name), "%s%s", "cpu_", filename);

  wav wave = get_wav(filename);

  real *data = (real*)malloc(wave.len * sizeof(real));
  int samples = FFTSAMPLE;
  int howmany = (int)(wave.len / samples);
  int i;
  struct timeval start, end;
  long mtime, secs, usecs;

  for(i = 0; i < wave.len; i ++)
    data[i] = (real)wave.data[i] / samples;

  gettimeofday(&start, NULL);

  complex *f_ft = forward(data, samples, howmany);
  filter(f_ft, (int)(samples / 2) + 1, howmany);
  real    *b_ft = backward(f_ft, samples, howmany);

  gettimeofday(&end, NULL);
  secs = end.tv_sec - start.tv_sec;
  usecs = end.tv_usec - start.tv_usec;
  float etime = ((secs) * 1000 + usecs/1000.0);
  
  printf("Elapsed time: %.2f \n", etime);
  
  for(i = 0; i < wave.len; i ++) {
    wave.data[i] = (wav_type)(b_ft[i]);
  }
  write_wav(wave, out_name);
  
  return 0;
}
