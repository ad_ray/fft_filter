all: gpu_fft cpu_fft


gpu_fft: main.cu
	nvcc -ccbin /usr/bin/gcc-4.6 main.cu -arch=sm_20 -lcufft -o gpu_fft

cpu_fft: main.c
	gcc main.c -lfftw3f -lm -o cpu_fft

clean:
	rm -rf *.o gpu_fft cpu_fft
